from matplotlib import path
from matplotlib.figure import Figure
import torch
import matplotlib.pyplot as plt 
import numpy as np
from sklearn.metrics import ConfusionMatrixDisplay
import os

from torch.utils.data import Dataset


def plot_train_loss(losses: list, path_filename: str = "../figures/training_loss.png") -> None:
    """
    Plot the training loss as recorded.
    ---
    Input:
        losses (list): Nested list of losses per epoch of training.
        Optional: 
            path_filename (str): Path towards where to save the plot
    """

    f, ax = plt.subplots(1, 1)

    loss_mean_epoch = np.array([ np.mean(e) for e in losses ])
    loss_std_epoch = np.array([ np.std(e) for e in losses ])

    # Epochs error "area"
    ax.plot(loss_mean_epoch)
    ax.fill_between(np.arange(len(losses)), loss_mean_epoch - loss_std_epoch, loss_mean_epoch + loss_std_epoch, alpha=0.2)

    ax.set_title("Mean loss across epochs of training")
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Mean Loss')

    save_fig(f, path_filename)

def plot_conf_mat(conf_mat: np.ndarray, classes: list, path_filename: str = "../figures/conf_matrix.png") -> None:
    """
    Plot the confusion matrix on evaluation.
    ---
    Input:
        conf_mat (np.ndarray): Confusion matrix on test data.
        classes (list): List of diagnostic classes.
        Optional: 
            path_filename (str): Path towards where to save the plot
    """

    f, ax = plt.subplots(1, 1)
    plot = ConfusionMatrixDisplay(conf_mat, display_labels=classes).plot(ax=ax)
    ax.set_title("Confusion Matrix of the Convolutional Classifier")
    save_fig(f, path_filename)

def plot_cam(cam: np.ndarray, recordings: torch.Tensor, path_filename: str = "../figures/cam.png") -> None:
    """
    Plot the confusion matrix on evaluation.
    ---
    Input:
        cam (np.ndarray): Class activation map
        recordings (torch.Tensro): Raw tensor to draw
        Optional: 
            path_filename (str): Path towards where to save the plot
    """
    f, ax = plt.subplots(1, 1)
    ax.plot(recordings[0][0])
    ax.imshow(cam[np.newaxis,:], cmap="plasma", aspect="auto")
    save_fig(f, path_filename)

def plot_recording(recordings: Dataset, n_sample: int = 0, n_channel: int = 0, path: str = "../figures/") -> None:
    f, ax = plt.subplots(1, 1)
    rec, _ = recordings[n_sample]
    chan = rec[n_channel]
    ax.plot(chan)
    ax.set_title("Example of Recording")
    ax.set_xlabel("time (ms)")
    ax.set_ylabel("mV")
    path_filename = path + f"record_{n_sample}_{n_channel}"
    save_fig(f, path_filename)


def save_fig(fig: Figure, path: str) -> None:
    os.makedirs(os.path.dirname(path), exist_ok=True)
    fig.savefig(path)

