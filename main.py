#!/bin/python3

import argparse
import os

import torch
import torch.nn as nn
import torch.optim as optim
from torchsummary import summary

from dataloader import get_data_tensors, get_classes

from models import train_model, eval_accuracy, eval_conf_matrix, ConvModel

from cam import compute_cam, compute_naive_cam

from plotting import plot_recording, plot_train_loss, plot_conf_mat, plot_cam

parser = argparse.ArgumentParser()
parser.add_argument('--epochs', default=15, type=int)
parser.add_argument('--seed', default=42, type=int)
parser.add_argument('--batch-size', default=5, type=int)
parser.add_argument('--lr', default=0.001, type=float)
parser.add_argument('--momentum', default=0.9, type=float)
parser.add_argument('--test-size', default=0.4, type=float)
# flags for performing training
parser.add_argument('--train', action=argparse.BooleanOptionalAction, default=True)

args = parser.parse_args()

torch.manual_seed(args.seed)

# Data folder should be above root
print("Importing the data...")
train_data, test_data = get_data_tensors("../data/", args.batch_size, args.seed, args.test_size)

# Create model
# /!\ you can change default values with care (check integer values) /!\
model = ConvModel()
# load pretrained model if no training
if not args.train:
    model.load_state_dict(torch.load("../model/last_trained_model.pth"))

# Establish the loss
criterion = nn.CrossEntropyLoss()
# Define optimizer
otpimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)

# Pretty printer for the model (keras like)
print("1D convolutional model:")
summary(model, (12, 1000), args.batch_size)

# Save the trained_model
if args.train:
    print("Training...")
    # Losses from training
    losses_epochs = train_model(args.epochs, train_data, model, otpimizer, criterion)
    plot_train_loss(losses_epochs)
    os.makedirs("../model", exist_ok=True)
    torch.save(model.state_dict(), "../model/last_trained_model.pth")


# Compute accuracy
corr, total = eval_accuracy(model, test_data)
print(f"Accuracy on test dataset: {corr / total:.5f}")

# Compute confusion matrix
conf_mat = eval_conf_matrix(model, test_data)
plot_conf_mat(conf_mat, get_classes())

# Attempt at CAM
recordings, diagx = test_data.dataset[42]
recordings = recordings.view(1, 12, 1000)
cam = compute_cam(model, recordings)
plot_cam(cam, recordings)

naive_cam = compute_naive_cam(model, recordings, diagx)
plot_cam(naive_cam, recordings, path_filename="../figures/naive_cam.png")

# plot
plot_recording(train_data.dataset, 12, 0)
