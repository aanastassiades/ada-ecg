import sys

def progress_bar(percent: float, loss: float, epoch: int, need_flush: bool):
    """
    Pretty progress bar to show training status (Keras like).
    ---
    Input:
        percent (float): Progress completion percentage
    ---
    Output:
        None
    """
    sys.stdout.write("\r")
    i = int(percent // 2)
    sys.stdout.write("epoch %-3s: [%-50s] %-3s%% - loss: %.5f" % (str(epoch), '='*(i-1)+'>', str(int(percent)), loss))
    if need_flush: 
        sys.stdout.flush()
