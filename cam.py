import warnings

import numpy as np

import torch
import torch.nn as nn
from torchvision.models.feature_extraction import create_feature_extractor

from scipy.interpolate import CubicSpline

def compute_cam(model: nn.Module, recordings: torch.Tensor) -> np.ndarray:
    """
    Compute BAD class activation map
    ---
    Input: 
        model (nn.Module): Model to cal on.
        recordings (torch.Tensor): Raw recordings.
    ---
    Output: 
        proj_norm (np.ndarray): The BAD CAM.
    """
    with torch.no_grad():
        # ignore depreciation warnings from feature extractor
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            feature_extractor = create_feature_extractor(model, ["conv_hidden"])

        # removing channels via sommation
        part_out = torch.flatten(feature_extractor(recordings)["conv_hidden"])
        dim_init = np.linspace(0, part_out.size()[0], num=part_out.size()[0])
        spl = CubicSpline(dim_init, part_out)
        dim_new = np.linspace(0, part_out.size()[0], num=1000)        
        proj = spl(dim_new)

        proj_norm = (proj-np.min(proj))/(np.max(proj)-np.min(proj)) 

        return proj_norm

def compute_naive_cam(model: nn.Module, recordings: torch.Tensor, diagx: torch.Tensor) -> np.ndarray:
    """
    Compute naive class activation map
    ---
    Input: 
        model (nn.Module): Model to cal on.
        recordings (torch.Tensor): Raw recordings.
        diagx (torch.Tensor): diagx labels.
    ---
    Output: 
        distance_mat (np.ndarray): Better CAM interp with numpy.
    """
    # stadart output
    model_out = model(recordings)
    # label number
    label = diagx.item()
    # initialisation of the weight matrix
    weight = np.ones((5, 5))
    weight[:, label] = 4
    weight[label, :] = 4
    weight[label, label] = 8
    weight /= (np.linalg.norm(weight))
    weight_t = torch.tensor(weight).float()

    dists = []

    for i in range(100):
        # calculate perturbed score
        perturbed_rec = recordings.clone()
        perturbed_rec[:, :, i*10:(i+1)*10] = 0
        perturbed_out = model(perturbed_rec)

        # difference
        diff = model_out - perturbed_out
        # Normalization
        dist = torch.matmul(torch.matmul(diff, weight_t), diff.T)
        dists.append(dist.item())

    # interpolation to have the good size
    return np.interp(np.linspace(0, 100, 1000), np.linspace(0, 100, 100), np.array(dists))

