MAIN := ./main.py
DATA := ../data/

.PHONY: all train

all: 
	$(MAIN) --no-train

train:
	$(MAIN)
