from pandas._libs.lib import is_integer
import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import pandas as pd

from torcheval.metrics import MulticlassConfusionMatrix
from torch.utils.data import DataLoader

from progress_bar import progress_bar

class ConvModel(nn.Module):
    """
    Basic convolution model.
    """
    def __init__(self, 
                 input_size: int = 1000,
                 chan_input: int = 12, 
                 chan_conv_init: int = 24, 
                 kern_conv_init: int = 11,
                 chan_conv_hidd: int = 36, 
                 kern_conv_hidd: int = 14,
                 kern_pool: int = 5,
                 linear_1: int = 256,
                 linear_2: int = 64,
                 n_classes: int = 5,
                 ):
        """
        Initialization of the convolution model.
        ---
        Input:
            input_size (int): Size of the dimension (time).
            chan_input (int): Number of channels in the input. 
            chan_conv_init (int): Number of channels in first convolutionnal layer. 
            kern_conv_init (int): Kernel of the first convolutionnal layer.
            chan_conv_hidd (int): Number of channels in second convolutionnal layer. 
            kern_conv_hidd (int): Kernel of the second convolutionnal layer.
            kern_pool (int): Kernel size of the max pooling.
            linear_1 (int): Dimension size of the first linear layer.
            linear_2 (int): Dimension size of the second linear layer..
            n_classes (int): Number of diagnostic class to learn.
        """
        super(ConvModel, self).__init__()

        # helpers to calculate the correct sizes
        calc_out_conv_size = lambda in_size, k: in_size - k + 1
        calc_out_pool_size = lambda in_size, k: (in_size - k)/k + 1
        # silly function to throw an error inside a lambda 
        check_dims = lambda dim: int(dim) if dim.is_integer() else (_ for _ in ()).throw(Exception("Problem with conv/pooling dims")) 

        # init conv layers
        self.conv_init = nn.Conv1d(chan_input, chan_conv_init, kern_conv_init)         
        self.conv_hidden = nn.Conv1d(chan_conv_init, chan_conv_hidd, kern_conv_hidd) 

        # calculate the dimensions
        out_init_size = calc_out_conv_size(input_size, kern_conv_init)
        in_hid1_size = check_dims(calc_out_pool_size(out_init_size, kern_pool))

        out_hid1_size =  calc_out_conv_size(in_hid1_size, kern_conv_hidd)
        out = check_dims(calc_out_pool_size(out_hid1_size, kern_pool))


        # init linear 
        self.linear_1 = nn.Linear(out * chan_conv_hidd, linear_1)
        self.linear_2 = nn.Linear(linear_1, linear_2)
        self.linear_3 = nn.Linear(linear_2, n_classes)

        # init pooling
        self.pooling = nn.MaxPool1d(kern_pool)

    def forward(self, x):
        """
        Compute the forward pass on the network
        ---
        Input:
            x (np.array((B, 1000, 12))): B batch size, vector input (recordings).
        ---
        Output:
            out (np.array((B, 5))): B batch size, vector output (predictions).
        """
        x = self.pooling(F.relu(self.conv_init(x))) 
        x = self.pooling(F.relu(self.conv_hidden(x)))
        x = torch.flatten(x, 1)
        x = F.relu(self.linear_1(x))
        x = F.relu(self.linear_2(x))
        x = F.relu(self.linear_3(x))
        return x



def train_model(n_epochs: int, data: DataLoader, model: nn.Module, optimizer: torch.optim.Optimizer, criterion: nn.Module) -> list:
    """
    Training model on data
    ---
    Input:
        n_epochs (int): Number of iterations over the dataset
        data (DataLoader): Training data
        model (nn.Module): Model to train
        optimizer (torch.optim.Optimizer): Pytorch optimizer
        criterion (function): Loss function
    ---
    Output:
        losses_epochs (list): Losses saved during training
    """
    losses_epochs = []
    for epoch in range(n_epochs):
        losses = []
        data_len = len(data)
        summed_loss = 0.0
        for i, d in enumerate(data):
            inputs, labels = d

            # reset grad
            optimizer.zero_grad()

            # forward - backward - update
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            summed_loss += loss

            # Keras like progress bar with mean loss calculated on the fly
            progress_bar(100*(i+1)/data_len, summed_loss/i, epoch+1, (i != (data_len - 1)))
            losses.append(float(loss))

        print() # new line for a new progress_bar
        losses_epochs.append(losses)
    return losses_epochs

def eval_accuracy(model: nn.Module, data: DataLoader) -> tuple[float, float]:
    """
    Evaluating the model on test data with accuracy
    ---
    Input:
        model (nn.Module): Model to train
        data (DataLoader): Training data
    ---
    Output:
        correct (float): numbers of correct predictions
        total (float): count the total number of data points evaluated
    """
    correct, total = 0, 0
    with torch.no_grad():
        for d in data:
            inputs, labels = d
            outputs = model(inputs)
            _, predicted = torch.max(outputs, 1)
            total += labels.size(0) 
            # difference between prediction and labels
            correct += (predicted == labels).sum().item()

    return correct, total

def eval_conf_matrix(model: nn.Module, data: DataLoader, n_classes: int = 5) -> np.ndarray:
    """
    Evaluating the model on test data with confusion matrix
    ---
    Input:
        model (nn.Module): Model to train
        data (DataLoader): Training data
        n_classes (int): Number of classes outputted
    ---
    Output:
        conf_mat (np.ndarray): Confusion matrix from pytorch torcheval package
    """
    conf_mat = MulticlassConfusionMatrix(n_classes)
    with torch.no_grad():
        for d in data:
            input, target = d
            output = model(input)
            conf_mat.update(output, target)
    
    c = conf_mat.compute().numpy()
    c_norm = c / c.astype(float).sum(axis=1)

    return c_norm
