# Advanced Data Analysis Project: ECG Analysis with Deep Learning
### Antoine Anastassiades

Analyze ECG from [PTB-XL](https://physionet.org/content/ptb-xl/1.0.3/) database using appropriate Deep Learning techniques.
**The main goal of my analysis would be to build a classifier for the diagnostic superclasses supplied in the dataset**.
First I want to try Convolutional Neural Networks (CNN), without taking into account the temporal nature of the signals.
Next, I would like to try Recurrent Neural Networks, and try appropriate loss functions such as the Time Warp loss.

Some statistics about the dataset and the records: 
- 21799 ECG records from 18869 patients
- 12 leads (channels) ECG
- 10 seconds long
- 5 superclasses of diagnostic

---
## Setup 
You will need some packages such as `wfdb`, `torch`, `sklearn` and `argparse` in addition to the classical `numpy`, `pandas` and `matplotlib`.
Most of the output directories will be created in the parent directory of this project. 

## Loading the data
The dataset can be downloaded from [physionet website](https://physionet.org/content/ptb-xl/1.0.3/).
It should be moved inside the data directory (**in the parent directory of this project (`../data`)**) and renamed appropriately (`ptb-xl/`).
I used only the superclass diagnostics for this project and removed all recordings having more than one superclass diagnostic.

## Models and training
### Convolutional Neural Network
2 Convolutional layers and three Linear layers. Hyperparameters not fine-tuned.

## Results
Relatively good results were achieved despite lack of tuning. 75% accuracy with 15 epochs training on test data.

## Conclusion and perspectives
I would have liked to tune a bit more the current model. Moreover, the CAM was really close to work and would have made really cool visualization.
Furthermore, RNN would have been interesting to work with on this type of data.

---
## TODO
- [x] Deal with minibatches
    - [x] Check what data type is needed for training pytorch models (Tensors?)
    - [x] Convert data (recordings) and labels (classes) to tensors at the last moment after converting them to np.ndarray
    - [x] from_numpy, TensorDataset, DataLoader
    - [x] Put tensor conversion inside helper in `dataloader.py`
    - [x] Deal with correct type hints for data in train
    - [x] Set shuffle to True for DataLoader
- [x] Deal with warning on tensor data type
- [x] Do evaluation
- [x] Use argparse for parameters 
- [x] Do proper function and class documentation/comments
- [x] Pretty printer of the model with `torchsummary` package
- [x] Do plotting
- [x] Reload previously trained model
- [x] Better plotting
    - [x] Mean is ok, raw is a mess, no more raw
    - [x] Check to plot recordings from raw data
- [x] Pass arguments to model classes instead of hardcode
- [x] Makefile 
    - [x] running
- [x] Better evaluations - Confusion Matrix
- [ ] Class Activation Map 
    - [x] Naive approach done 
    - [ ] Better weight initialization
- [ ] Recursive Neural Network model
- [ ] Better DataLoader (inheritance from `Dataset` class)
