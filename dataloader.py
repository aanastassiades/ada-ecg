# os manipulations
import os
# basic data structures and functions
import numpy as np
import pandas as pd

import torch
from torch.utils.data import DataLoader 
from torch.utils.data.dataset import TensorDataset

from sklearn.model_selection import train_test_split

# dataset specific 
import wfdb 
import ast

def get_data_tensors(path: str, batch_size: int, seed: int, test_size: float) -> tuple[DataLoader, DataLoader]:
    """
    Main entry point for loading the data.
    Load data as ndarray and dataframes, convert them to DataLoader objects.
    ---
    Input:
        path (str): Path to the dataset, dataset should be named "ptb-xl/"
    --- 
    Output: 
        train_data (DataLoader): training part of the dataset
        test_data (DataLoader): testing part of the dataset
    """
    X, labels = loader(path)

    X_train, X_test, y_train, y_test = train_test_split(X, labels, test_size=test_size, random_state=seed)

    X_train_tensor = torch.from_numpy(X_train).float()
    y_train_tensor = torch.from_numpy(y_train).long()

    X_test_tensor = torch.from_numpy(X_test).float()
    y_test_tensor = torch.from_numpy(y_test).long()

    wrap_train_data = TensorDataset(X_train_tensor, y_train_tensor)
    wrap_test_data = TensorDataset(X_test_tensor, y_test_tensor)

    train_data = DataLoader(wrap_train_data, batch_size=batch_size, shuffle=True)
    test_data = DataLoader(wrap_test_data, batch_size=batch_size, shuffle=True)
    return (train_data, test_data)

def get_classes() -> list:
    """
    Return the diagnostic superclasses
    """
    return ["NORM", "MI", "STTC", "CD", "HYP"]


def loader(path: str) -> tuple[np.ndarray, np.ndarray]:
    """
    Load the data with wfdb package and store it for easier access.
    ---
    Input:
        path (str): Path to the dataset, dataset should be named "ptb-xl/"
    --- 
    Output: 
        X (np.ndarray): ECG recordings sharing index with Y
        labels_dig (np.ndarray): Superclasses labels diditized
    """
    output_path = path + "preloaded/ptbxl/"
    if os.path.exists(output_path):
        X = np.load(output_path + "X.npy")
        Y = pd.read_csv(output_path + "Y.csv")
        # Convert strings to previous lists
        Y.diagnostic_superclass = Y.diagnostic_superclass.apply(ast.literal_eval)
    else:
        X, Y = load_data(path)
        os.makedirs(output_path)
        np.save(output_path + "X", X)
        Y.to_csv(output_path + "Y.csv")

    # Remove multiple diagnostic superclasses
    Y = Y[Y.diagnostic_superclass.apply(len) == 1].explode("diagnostic_superclass")
    # Get the corresponding recordings
    
    X = X[Y.index]
    # reshape to put channels in the right spot
    X_resh = np.transpose(X, (0,2,1))
    #small check
    assert (X_resh[:, 5, :] == X[:, :, 5]).all()
    
    labels = Y.diagnostic_superclass.values
    conv = {
            "NORM": 0, # Normal ECG
            "MI":   1, # Myocardial Infarction
            "STTC": 2, # ST/T change
            "CD":   3, # Conduction Disturbance
            "HYP":  4, # Hypertrophy
            }
    labels_dig = np.vectorize(conv.get)(labels)
    return (X_resh, labels_dig)


def load_data(path: str) -> tuple[np.ndarray, pd.DataFrame]:
    """
    Load data from original dataset (see example_physionet.py)
    ---
    Input:
        path (str): Path to the dataset, dataset should be named "ptb-xl/"
    --- 
    Output: 
        X (np.ndarray): ECG recordings sharing index with Y
        Y (df.DataFrame): ECG annotations sharing index with X
    """
    db_path = path + "ptb-xl"

    Y = pd.read_csv(db_path + 'ptbxl_database.csv')
    Y.scp_codes = Y.scp_codes.apply(lambda x: ast.literal_eval(x))

    X = load_wfdb_raw(Y, db_path)

    agg_df = pd.read_csv(db_path+'scp_statements.csv', index_col=0)
    agg_df = agg_df[agg_df.diagnostic == 1]
    Y['diagnostic_superclass'] = Y.scp_codes.apply(aggregate_diagnostic, args=(agg_df,))

    return (X, Y)

### Helper functions provided in ./data/ptb-xl/example_physionet.py ###

def load_wfdb_raw(df: pd.DataFrame, path: str) -> np.ndarray:
    """
    Load data from original dataset given the annotation data (see example_physionet.py)
    ---
    Input:
        path (str): Path to the dataset, dataset should be named "ptb-xl/"
    --- 
    Output: 
        X (np.ndarray): ECG recordings sharing index with Y
        Y (df.DataFrame): ECG annotations sharing index with X
    """
    data = [wfdb.rdsamp(path+f) for f in df.filename_lr]
    data = np.array([signal for signal, _ in data])
    return data

def aggregate_diagnostic(y_dic: dict, agg_df: pd.DataFrame) -> list:
    """
    Convert diagnostic annotations to superclasses diagnostic
    ---
    Input:
        y_dic (dict): original diagnostics and annotations
        agg_df (pd.DataFrame): conversion table from diagnostics to superclasses
    --- 
    Output: 
        diagnostic_superclass (list): list of all the superclasses for a given recording
    """
    diagn_supcls = []
    for key in y_dic.keys():
        if key in agg_df.index:
            diagn_supcls.append(agg_df.loc[key].diagnostic_class)
    return list(set(diagn_supcls))
